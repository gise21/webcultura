<?php

function Consulta($categoria, $nombre, $prov = true)
{
    include("db.php");
    $departamento = $_POST['departamento'];
    $provincia = $_POST['provincia'];

    if ($prov) :

        $query = "SELECT A.nombre, A.direccion, A.telefono, A.mail, D.dep_name, P.prov_name
        FROM Actividades AS A
        INNER JOIN departamento AS D ON D.dep_id = A.dep_id
        INNER JOIN provincias AS P ON P.prov_id = '$provincia'  
        WHERE A.dep_id = '$departamento' AND A.cat_id = '$categoria'";

    else : // Monumentos se relaciona por provincia con la tabla pivot y no por departamento

        $query = "SELECT A.nombre, A.direccion, A.telefono, A.mail, D.dep_name, P.prov_name
        FROM Actividades AS A
        INNER JOIN departamento AS D ON D.dep_id = '$departamento'
        INNER JOIN provincias AS P ON P.prov_id = '$provincia'
        WHERE A.dep_id = '$provincia' AND A.cat_id = '$categoria'";

    endif;

    $result = mysqli_query($conn, $query);
    $row_cnt = mysqli_num_rows($result);

    if ($result && $row_cnt > 1) : // Si no hay resultado no imprimo la tabla
    ?>

<h2 id="<?= $nombre ?>"><?= $nombre ?></h2>
    <a href="#arriba">Volver arriba</a>
        <table style="width: 50%" border=1>
            <thead>
                <tr>
                    <th>Provincia</th>
                    <th>Departamento</th>
                    <th>Nombre</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($row = mysqli_fetch_assoc($result)) :
                ?>
                    <tr>
                        <td><?= $row['prov_name'] ? $row['prov_name'] : "s/d" ?></td>
                        <td><?= $row['dep_name'] ? $row['dep_name'] : "s/d" ?></td>
                        <td><?= $row['nombre'] ? $row['nombre'] : "s/d" ?></td>
                        <td><?= $row['direccion'] ? $row['direccion'] : "s/d" ?></td>
                        <td><?= $row['telefono'] ? $row['telefono'] : "s/d" ?></td>
                        <td><?= $row['mail'] ? $row['mail'] : "s/d" ?></td>
                    </tr>
            <?php
                endwhile;
            endif;
            ?>
            </tbody>
        </table>

    <?php

}

    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Listar</title>
    </head>

    <body>

        <h1 id="arriba">Seleccione la categoría deseada:</h1>
        <a href="index.php">Volver a la sección de búsqueda</a>
        <br>
        <br>
        <a href="#Cines">Cines</a>
        <a href="#Monumentos">Monumentos</a>
        <a href="#Museos">Museos</a>
        <a href="#Teatros">Teatros</a>

        <?php
        Consulta(1, "Cines");
        echo "<br>";
        Consulta(2, "Monumentos", false);
        echo "<br>";
        Consulta(3, "Museos");
        echo "<br>";
        Consulta(4, "Teatros");
        ?>

    </body>

    </html>